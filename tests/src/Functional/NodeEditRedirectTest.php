<?php

namespace Drupal\Tests\node_edit_redirect\Functional;

use Drupal\Core\Url;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\node\NodeInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests that the node edit redirect works.
 *
 * @group node_edit_redirect
 */
class NodeEditRedirectTest extends BrowserTestBase {
  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node_edit_redirect',
    'locale',
    'content_translation',
  ];

  /**
   * Use the standard profile.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin);

    // Create French and Spanish languages.
    $this->addLanguage('fr');
    $this->addLanguage('es');

    // Set prefixes to en and fr.
    $this->drupalGet('admin/config/regional/language/detection/url');
    $this->submitForm([
      'prefix[en]' => 'en',
      'prefix[es]' => 'es',
      'prefix[fr]' => 'fr',
    ], 'Save configuration');
    // Set up URL and language selection page methods.
    $this->drupalGet('admin/config/regional/language/detection');
    $this->submitForm([
      'language_interface[enabled][language-url]' => 1,
    ], 'Save settings');
    // Turn on content translation for pages.
    $this->drupalGet('admin/structure/types/manage/page');
    $this->submitForm([
      'language_configuration[language_alterable]' => 1,
      'language_configuration[content_translation]' => 1,
    ], 'Save content type');
  }

  /**
   * Test that the redirect works.
   */
  public function testRedirect() {
   // Create Basic page in Spanish.
   $node_title = $this->getRandomGenerator()->name(8);
   $node_body =  $this->getRandomGenerator()->paragraphs(2);
   $node = $this->createPage($node_title, $node_body, 'es');

   // Visit the English page for a Spanish node.
   $this->assertRedirect('/en/node/' . $node->id() . '/edit', '/es/node/' . $node->id() . '/edit');
   $languages = \Drupal::languageManager()->getLanguages();

    $this->drupalGet(Url::fromRoute('entity.node.edit_form', ['node' => $node->id()], [
      'absolute' => TRUE,
      'language' => $languages['en'],
    ])->toString());

   // Assert that we redirected to the Spanish edit page.
   $this->assertRedirect($this->getUrl(), '/es/node/' . $node->id() . '/edit');
  }

  /**
   * Installs the specified language.
   *
   * @param $language_code
   *   The language code to check.
   */
  protected function addLanguage($language_code) {
    $language = ConfigurableLanguage::createFromLangcode($language_code);
    $language->save();
  }

  /**
   * Creates a "Basic page" in the specified language.
   *
   * @param $title
   *   The title of a basic page in the specified language.
   * @param $body
   *   The body of a basic page in the specified language.
   * @param $language
   *   (optional) Language code.
   *
   * @return \Drupal\node\NodeInterface
   *   A node entity.
   */
  protected function createPage($title, $body, $language = NULL) {
    $edit = [];
    $edit['title[0][value]'] = $title;
    $edit['body[0][value]'] = $body;
    if (!empty($language)) {
      $edit['langcode[0][value]'] = $language;
    }
    $assert_session = $this->assertSession();
    $this->drupalGet('node/add/page');
    $this->submitForm($edit, 'Save');
    $assert_session->pageTextContainsOnce(t('Basic page :title has been created.', [':title' => $title]));

    // Check to make sure the node was created.
    $node = $this->drupalGetNodeByTitle($title);
    $this->assertTrue(($node instanceof NodeInterface), 'Node found in database.');

    return $node;
  }

  /**
   * Visits a path and asserts that it is a redirect.
   *
   * @param string $path
   *   The request path.
   * @param string $expected_destination
   *   The path where we expect it to redirect. If NULL value provided, no
   *   redirect is expected.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function assertRedirect(string $path, string $expected_destination) {
    // Always just use getAbsolutePath() so that generating the link does not
    // alter special requests.
    $url = $this->getAbsoluteUrl($path);
    $this->getSession()->visit($url);

    // Ensure that any changes to variables in the other thread are picked up.
    $this->refreshVariables();

    $assert_session = $this->assertSession();
    $assert_session->addressEquals($expected_destination);
  }

}
